# CSV File Reader

Backend:

    - Go to config/connection.js to set mysql database properties.
    - npm install

    File Structure:

        - api/controllers: All the controllers
        - api/models: All the models
        - api/policies: Auth policies
        - api/services: All service files

        - config/routes: Public and Private Routes here
        - config/connection.js: MySql connection settings
        - config/index.js: Set port here

        Note: Set migrate property in config/index.js to true after first run. Setting it to false will create all the tables.

Frontend:
    - set the server url in src/app/services.config.service.ts

        

