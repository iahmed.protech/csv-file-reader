const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const tableName = 'invoices';

const invoice = sequelize.define('invoices', {
    invoice_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    invoice_amount: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: false,
    },
    due_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
    },
    invoice_sell_price: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: false
    }
  }, { tableName });
  
  // eslint-disable-next-line
  invoice.prototype.toJSON = function () {
    const values = { ...this.get() }; // Object.assign({}, this.get());
    return values;
  };
  
  module.exports = invoice;
  