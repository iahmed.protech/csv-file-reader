/**
 * third party libraries
 */
const bodyParser =  require('body-parser'); // require('body-parser');
// require('body-parser');
 const express = require('express');
 const helmet = require('helmet');
 const http = require('http');
 const mapRoutes = require('express-routes-mapper');
 const cors = require('cors');
 const multer = require('multer');
 const fs = require('fs');
 const csv = require('fast-csv');
 /**
  * server configuration
  */
 const config = require('../config/');
 const dbService = require('./services/db.service');
 // const auth = require('./policies/auth.policy');
 
 // environment: development, staging, testing, production
 const environment = 'development';
 // process.env.NODE_ENV;
 
 // setting folder for images/audio uploads
 const upload = multer({ dest: 'files/' });
 // const upload = multer({ storage });
 /**
  * express application
  */
 const app = express();
 const server = http.Server(app);
 const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'api/controllers/');
 // const mappedAuthRoutes = mapRoutes(config.privateRoutes, 'api/controllers/');
 const DB = dbService(environment, config.migrate).start();
 
 // allow cross origin requests
 // configure to only allow requests from certain origins
 app.use(cors());
 
 
 // secure express app
 app.use(helmet({
     dnsPrefetchControl: false,
     frameguard: false,
     ieNoOpen: false,
 }));
 
 app.use((req, res, next) => {
     res.header('Access-Control-Allow-Origin', '*');
     res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
     res.header('Access-Control-Allow-Headers', 'Content-Type');
     next();
 });
 
 // parsing the request bodys
 // app.use(bodyParser.urlencoded({ extended: false }));
 
 // app.use(bodyParser.json());
 
 app.use(bodyParser.json({ limit: '50mb' }));
 app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
 
 // securing private routes with jwt authentication middleware
 // app.all('/private/*', (req, res, next) => auth(req, res, next));
 
 // fill routes for express application
 app.use('/public', mappedOpenRoutes);
 // app.use('/private', mappedAuthRoutes);

 // set static content
app.use('/files/', express.static('files'));

 // route for uploading images/audio on the server
// app.post('/mediaupload', upload.single('file'), (req, res) => {
    
//     const mediaFile = req.file;
//     console.log(mediaFile);
//     let originalName = mediaFile.originalname;
//     originalName = originalName.replace(/\s/g, '');
//     const filePath = `files/${originalName}`;
//     fs.rename(`files/${fileName}`, `files/${originalName}`, (err) => {
//                     if (err) {
//                         console.log(err);
//                         res.status(500).json({ err });
//                     } else {
//                         res.status(200).json({ filePath });
//                     }
//                 });
//     // const dirPath = `uploads/${userId}`;
//     // fs.exists(filePath, (exists) => {
//     //     if (exists) {
//     //         fs.rename(`files/${fileName}`, `files/${originalName}`, (err) => {
//     //             if (err) {
//     //                 console.log(err);
//     //                 res.status(500).json({ err });
//     //             } else {
//     //                 res.status(200).json({ filePath });
//     //             }
//     //         });
//     //     } else {
//     //         fs.rename(`files/${fileName}`, `files/${originalName}`, (err) => {
//     //             if (err) {
//     //                 console.log(err);
//     //                 res.status(500).json({ err });
//     //             } else {
//     //                 res.status(200).json({ filePath });
//     //             }
//     //         });
//     //     }
//     // });
// });

app.post('/mediaupload', upload.single("file"), (req, res) => {
    try {
        if(req.file == undefined) {
            return res.status(400).send({
                message: "Please upload CSV File!"
            });
        }

        let csvData = [];
        let filePath = `files/${req.file.filename}`;
        fs.createReadStream(filePath)
            .pipe(csv.parse({ headers: true }))
            .on("error", (error) => {
                throw error.message;
            })
            .on("data", (row) => {
                console.log(row);
                csvData.push(row);
            })
            .on("end", () => {
                console.log('stream ended');
            })
    } catch(error) {
        console.log("Error: ", error);
        res.status(500).send({
            message: "Error uploading file: " + req.file.originalname,
        })

    }
});
 

 // starts the server
 server.listen(config.port, () => {
     if (environment !== 'production'
         && environment !== 'development'
         && environment !== 'testing'
     ) {
         console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
         process.exit(1);
     } else {
         console.info('running!');
     }
     return DB;
 });
 