const bcrypt = require('bcrypt-nodejs'); // library to generate hash


const bcryptService = () => {
    // generate hashed password
    const password = (user) => {
        const salt = bcrypt.genSaltSync();
        const hash = bcrypt.hashSync(user.password, salt);

        return hash;
    };

    // compare the password
    const comparePassword = (pw, hash) => (
        bcrypt.compareSync(pw, hash)
    );

    return {
        password,
        comparePassword,
    };
};

module.exports = bcryptService;