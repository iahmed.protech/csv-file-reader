const jwt = require('jsonwebtoken');

const secret = 'mysecret'; // secret key to generate the token.

const authService = () => {
    const issue = (payload) => jwt.sign(payload, secret, { expiresIn: 10800 }); // issue/generate new token
    const verify = (token, cb) => jwt.verify(token, secret, {}, cb); // verify the token

    return {
        issue,
        verify,
    };
};

module.exports = authService;