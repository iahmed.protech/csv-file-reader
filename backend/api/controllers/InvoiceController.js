/* eslint-disable max-len */
const { QueryTypes, Op } = require('sequelize');
// const bcryptService = require('../services/bcrypt.service');
const sequelize = require('../../config/database');
const fs = require('fs');
const path = require('path');
const invoice = require('../models/invoice');
const XLSX = require('xlsx');
const moment = require('moment');
const csv = require('fast-csv');
const multer = require('multer');

// const upload = multer({ dest: 'files/' });

// SET STORAGE
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'files')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
})


const InvoiceController = () => {

    
    const test = async (req, res) => {
        
        return res.status(200).json({ msg: 'success' });
    }
    getInvoiceList = async (req, res) => {
        try {
            const pageNum = req.params.pageNum;
            const totalRows =  parseFloat(req.params.totalRows);
            const invoiceList = await invoice.findAndCountAll({
                limit: totalRows,
                offset: parseInt(pageNum - 1) * totalRows
            });
            return res.status(200).send({ data: invoiceList });
        } catch(error) {
            console.log(error);
            return res.status(500).send({ message: error });
        }
    }
    const uploadFile = async (req, res) => {
        
        try {
            if(req.file == undefined) {
                return res.status(400).send({
                    message: "Please upload CSV File!"
                });
            }
    
            let csvData = [];
            let filePath = `files/${req.file.filename}`;
            var readStream = fs.createReadStream(filePath);
            readStream.pipe(csv.parse({ headers: true }))
                .on('headers', (headers) => {
                    if(checkCSVHeaders(headers) == false) {
                        readStream.destroy();
                        fs.unlink(filePath, function() {
                            // file deleted
                            return res.status(422).send({
                                message: "Invalid headers!"
                            });
                        })
                    }
                })
                .on("error", (error) => {
                    throw error.message;
                })
                .on("data", (row) => {
                    let tmp_obj = {};
                    tmp_obj.invoice_id = row.invoice_id;
                    tmp_obj.invoice_amount = parseFloat(row.invoice_amount);
                    tmp_obj.due_date = row.due_date;
                    csvData.push(tmp_obj);
                })
                .on("end", () => {
                    validateData(csvData).then((data) => {
                        saveIntoDB(data).then((newData) => {
                            fs.unlink(filePath, function() {
                                // file deleted
                                res.status(200).send({
                                    data: newData,
                                })
                            })
                        })
                    }).catch(error => {
                        fs.unlink(filePath, function() {
                            // file deleted
                            return res.status(422).send({
                                message: error
                            });
                        })
                        
                    })
                    
                })
        } catch(error) {
            console.log("Error: ", error);
            res.status(500).send({
                message: "Error uploading file: " + req.file.originalname,
            })
    
        }
    }
    
    function validateData(invoiceArr) {
        validatedArr = [];
        return new Promise((resolve, reject) => {
            try{
                for(i=0; i < invoiceArr.length; i++) {
                    if(validateRow(invoiceArr[i]) == false) { // if invoice row is valid
                        // throw 'Error: Invalid data at row number ' + (i + 1);
                        reject(`Error: Invalid data at row number ${(i + 1)}`)
                    }
                }
                resolve(invoiceArr);
            } 
            catch(error) {
                reject(error);
            }
            
        })
    }
    
    function validateRow(invoiceRow) {
        // if invoice_id and invoice_amount, due_date is not valid
        if(isNaN(invoiceRow.invoice_id) == false && isNaN(invoiceRow.invoice_amount) == false && moment(invoiceRow.due_date).isValid()) {
            return true;
        } else {
            return false;
        }
    }

    function checkCSVHeaders(headersArr) {
        if(headersArr.indexOf('invoice_id') > -1 && headersArr.indexOf('invoice_amount') > -1 && headersArr.indexOf('due_date') > -1) {
            return true;
        } else {
            return false;
        }
    }

    function saveIntoDB(invoiceArr) {
        
        return new Promise((resolve, reject) => {
            try {
                invoiceArr.forEach(invoice_obj => {
                    let coefficient = moment().diff(moment(invoice_obj.due_date), 'days') <= 30 ? 0.3 : 0.5;
                    invoice_obj.invoice_sell_price = parseFloat(invoice_obj.invoice_amount * coefficient);

                    invoice.findOne({ where: { invoice_id : parseInt(invoice_obj.invoice_id) } })
                    .then((found) => {
                        if(found) { // if invoice is already found, update it with new one
                            const dbResult = invoice.update({
                                invoice_amount: invoice_obj.invoice_amount,
                                due_date: invoice_obj.due_date,
                                invoice_sell_price: invoice_obj.invoice_sell_price,
                                updatedAt: moment()
                            },
                            {   
                                where: { invoice_id: invoice_obj.invoice_id },
                            }
                        ).then((result) => {
                            resolve(invoiceArr);
                        })
                        } else { // new invoice
                            const dbResult = invoice.create({
                                invoice_id: invoice_obj.invoice_id,
                                invoice_amount: invoice_obj.invoice_amount,
                                due_date: invoice_obj.due_date,
                                invoice_sell_price: invoice_obj.invoice_sell_price
                            });
                        }
                    }).then((result) => {
                        resolve(invoiceArr);
                    })
                });
                console.log(invoiceArr);
                // save into db
                // invoice.bulkCreate(invoiceArr).then((result) => {
                //     resolve(invoiceArr);
                // }).catch((err) => {
                //     console.log(err)
                //     reject(err);
                // })
                
            } catch(error) {
                reject(error);
            }
        })
    }
    return {
        getInvoiceList,
        uploadFile
    };
};

module.exports = InvoiceController;
