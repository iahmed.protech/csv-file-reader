
const privateRoutes = require('./routes/privateRoutes');
const publicRoutes = require('./routes/publicRoutes');

const config = {
    migrate: true, // if set to false, database will be deleted and created again. 
    privateRoutes,
    publicRoutes,
    port: process.env.PORT || '8000', // set port here on which server will listen to incoming requests
};

module.exports = config;