const development = {
    database: 'csv_invoice', //apicdr
    username: 'root', //apicdr
    password: 'admin!@#123ADMIN',
    host: 'localhost', //10.4.50.69
    dialect: 'mysql',
};

const testing = {
    database: 'csv_invoice',
    username: 'root',
    password: 'admin!@#123ADMIN',
    host: 'localhost',
    dialect: 'mysql',
};

const production = {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST || 'localhost',
    dialect: 'mysql',
};

module.exports = {
    development,
    testing,
    production,
};