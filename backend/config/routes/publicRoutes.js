const multer = require('multer');

const upload = multer({ dest: 'files/' });

const publicRoutes = {
    'GET /invoices/:pageNum/:totalRows': 'InvoiceController.getInvoiceList',
    'POST /mediaupload': {
        path: 'InvoiceController.uploadFile',
        middlewares: [
            upload.single("file")
       ],
    } 
};


   

module.exports = publicRoutes;