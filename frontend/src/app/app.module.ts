import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ConfigService } from './services/config.service';
import { ServerApiService } from './services/server-api.service';
import { PublicService } from './services/public.service';
import { CommonService } from './services/common.service';
import { NgbModule, NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DeviceDetectorModule } from 'ngx-device-detector';
import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { InvoicesComponent } from './invoices/invoices.component';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    // override hammerjs default configuration
    'swipe': { direction: Hammer.DIRECTION_ALL }
  }
}


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SideMenuComponent,
    TopBarComponent,
    InvoicesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    BrowserAnimationsModule,
    DeviceDetectorModule.forRoot(),
  ],
  providers: [
    ConfigService,
    ServerApiService,
    PublicService,
    CommonService,
    [{
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    }]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }