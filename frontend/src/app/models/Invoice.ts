export class Invoice {
    invoice_id: number;
    invoice_amount: number;
    due_date: Date;
    invoice_sell_price: number;
}