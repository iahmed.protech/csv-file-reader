import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  @ViewChild('menuUl', null) menuUl: ElementRef;
  permissionLevel: number = 0;
  username: string = '';
  constructor() { }

  ngOnInit() {
    // get the persmission level of the logged in user, use this to hide links that should only be visible to admin
  }
  onMouseEnter(e) {
    this.menuUl.nativeElement.classList.add('has-active');
    let elm = e.target;
    elm.classList.add('active');
  }
  onMouseLeave(e) {
    this.menuUl.nativeElement.classList.remove('has-active');
    let elm = e.target;
    elm.classList.remove('active');
  }
}
