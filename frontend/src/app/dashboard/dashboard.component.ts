import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Invoice } from '../models/Invoice';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private publicService: PublicService) { }
  
  @ViewChild('fileInput', null) fileInputElm: ElementRef;
  showLoader: boolean = false;
  
  fileData: File = null;
  
  csvError: string;
  invalidCSV: boolean;

  newInvoiceList: Invoice[] = [];

  ngOnInit() {
  }
  
  async onFileChange(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }
  
  /** Submit the file */
  async submitFile() {
    try {
      if(this.fileData != null) { // to make sure new file selected
        this.showLoader = true;
      
        const formData = new FormData();
        formData.append('file', this.fileData);
        this.showLoader = true;
        
        let invoiceData = await this.publicService.uploadMedia(formData);
        this.newInvoiceList = invoiceData.data;
  
        this.invalidCSV = false;
        this.showLoader = false;
        this.resetFileInput();
      } else {
        this.csvError = "Please upload file!";
        this.invalidCSV = true;
        this.newInvoiceList = [];
      }
    } catch(err) {
      this.resetFileInput();
      this.csvError = err.error.message;
      this.invalidCSV = true;
      this.showLoader = false;
      this.newInvoiceList = [];
    }
    
  }

  /*** reset the file input */
  resetFileInput() {
    this.fileInputElm.nativeElement.value = "";
    this.fileData = null;
  }
  
  

}
