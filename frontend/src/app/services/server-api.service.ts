import { HttpHeaders, HttpClient, HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// import { AuthService } from './auth.service';
import { ConfigService } from './config.service';


@Injectable()
export class ServerApiService {
    private static tokenKey = 'jwt.login';

    constructor(
        private configService: ConfigService,
        private http: HttpClient,
    ) { }

    

    

    private addAuthHeaderToOptions() {
        var headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        return headers;
    }

    private addFileAuthHeaderTopOptions() {
        // var accessToken = localStorage.getItem(ServerApiService.tokenKey);
        var headers = new HttpHeaders();
        headers = headers.set('enctype', 'multipart/form-data');
        // if (accessToken) {
        //     headers = headers.set('Authorization', `Bearer ` + accessToken);
        // }
        return headers;
    }

    private async isNotAuthorized() {
        // logout
    }


    private addPublicAuthHeaderToOptions() {
        var headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        return headers;
    }
    public getPublic<T>(url: string, httpOptions: object = {}): Promise<HttpResponse<T> & { body: T }> {
        const headers = this.addPublicAuthHeaderToOptions();
        return this.http.get<T>(this.configService.getServerBaseUrl() + url, {
            headers,
            ...httpOptions,
            observe: 'response',
        })
            .toPromise()
            .then((response: HttpResponse<T>) => {
                if (response.status === 401) {
                    this.isNotAuthorized();
                }
                if (response.status !== 200) {
                    // throw httpErrors(response.status, response.statusText);
                    return response as HttpResponse<T> & { body: T };
                }

                return response as HttpResponse<T> & { body: T };
            }, async (reason: unknown) => {
                const error = reason as HttpErrorResponse;
                if (error.status === 401) {
                    await this.isNotAuthorized();
                }
                // throw httpErrors(error.status, error.statusText);
                return reason as HttpResponse<T> & { body: T };
            })
            ;
    }

    public postPublic<T>(url: string, body: any | null = null, httpOptions: object = {}): Promise<HttpResponse<T> & { body: T }> {
        const headers = this.addPublicAuthHeaderToOptions();
        return this.http.post<T>(this.configService.getServerBaseUrl() + url, body, {
            headers,
            ...httpOptions,
            observe: 'response',
        })
            .toPromise()
            .then((response: HttpResponse<T>) => {
                if (response.status === 401) {
                    this.isNotAuthorized();
                }
                if (![200, 201].includes(response.status)) {
                    // throw httpErrors(response.status, response.statusText);
                    throw response as HttpResponse<T> & { body: T };
                }

                return response as HttpResponse<T> & { body: T };
            }, async (reason: unknown) => {
                const error = reason as HttpErrorResponse;
                if (error.status === 401) {
                    await this.isNotAuthorized();
                }
                // throw httpErrors(error.status, error.statusText);
                throw reason as HttpResponse<T> & { body: T };
            })
            ;
    }
    public putPublic<T>(url: string, body: any | null = null, httpOptions: object = {}): Promise<HttpResponse<T> & { body: T }> {
        const headers = this.addPublicAuthHeaderToOptions();
        return this.http.put<T>(this.configService.getServerBaseUrl() + url, body, {
            headers,
            ...httpOptions,
            observe: 'response',
        })
            .toPromise()
            .then((response: HttpResponse<T>) => {
                if (response.status === 401) {
                    this.isNotAuthorized();
                }
                if (![200].includes(response.status)) {
                    // throw httpErrors(response.status, response.statusText);
                    throw response as HttpResponse<T> & { body: T };
                }

                return response as HttpResponse<T> & { body: T };
            }, async (reason: unknown) => {
                const error = reason as HttpErrorResponse;
                if (error.status === 401) {
                    await this.isNotAuthorized();
                }
                // throw httpErrors(error.status, error.statusText);
                throw reason as HttpResponse<T> & { body: T };
            })
            ;
    }
    public postFile<T>(url: string, body: any | null = null, httpOptions: object = {}): Promise<HttpResponse<T> & { body: T }> {
        const headers = this.addFileAuthHeaderTopOptions();
        return this.http.post<T>(this.configService.getServerBaseUrl() + url, body, {
            headers,
            ...httpOptions,
            observe: 'response',
        })
            .toPromise()
            .then((response: HttpResponse<T>) => {
                if (response.status === 401) {
                    this.isNotAuthorized();
                }
                if (![200, 201].includes(response.status)) {
                    // throw httpErrors(response.status, response.statusText);
                    throw response as HttpResponse<T> & { body: T };
                }
                return response as HttpResponse<T> & { body: T };
            }, async (reason: unknown) => {
                const error = reason as HttpErrorResponse;
                if (error.status === 401) {
                    await this.isNotAuthorized();
                }
                // throw httpErrors(error.status, error.statusText);
                throw reason as HttpResponse<T> & { body: T };
            })
            ;
    }
}
