import { Injectable } from '@angular/core';
import { ServerApiService } from './server-api.service';
import { ConfigService } from './config.service';

@Injectable({
    providedIn: 'root'
})
export class PublicService {

    constructor(private serverApi: ServerApiService, private configService: ConfigService) { }

    async uploadMedia(file: any): Promise<any> {
        const { body: result } = await this.serverApi.postFile<{ data: any }>(`public/mediaupload`, file);
        return result;
    }

    async getInvoiceList(pageNum, totalRows): Promise<any> {
        const params = pageNum + `/` + totalRows;
        const { body: cdrList } = await this.serverApi.getPublic<{ data: any[] }>(`public/invoices/` + params);
        return cdrList;
      }
    getServerUrl(): string {
        return this.configService.getServerBaseUrl();
    }
}
