import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  convertBytes(bytes): string {
    if(bytes == null) {
      return '0';
    }
    const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if (bytes == 0) {
      return "n/a"
    }
  
    const i = Math.floor(Math.log(bytes) / Math.log(1024)); // parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
  
    if (i == 0) {
      return bytes + " " + sizes[i]
    }
  
    return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
    
  }
  convertBytesToGB(bytes) {
    return (bytes / (1000 * 1000 * 1000)).toFixed(2);
  }
}
