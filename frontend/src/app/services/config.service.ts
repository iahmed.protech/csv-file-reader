import { Inject, Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
    constructor() { }

    // set the backend server url here.
    getServerBaseUrl() {
        return 'http://localhost:8000/';
        // return 'https://apicdr.amtdev.it/';
    }
}