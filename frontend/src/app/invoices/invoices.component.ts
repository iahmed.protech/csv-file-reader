import { Component, OnInit } from '@angular/core';
import { Invoice } from '../models/Invoice';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

  constructor(private publicService: PublicService) { }
  showLoader: boolean = false;
  invoiceList: Invoice[] = [];

  totalRowsPerPage: number = 10;
  totalRowsOptions: number[] = [10,25,50];
  totalPageCountArr: number[];
  currentPageNumber: number = 1;
  totalPageCount: number;
  totalInvoiceCount: number;


  ngOnInit() {
    this.getInvoiceList();
  }

  async gotoPage(pageNum) {
    this.currentPageNumber = pageNum;
    await this.getInvoiceList();
  }

  async getInvoiceList() {
    try {
      this.showLoader = true;
      let result = await this.publicService.getInvoiceList(this.currentPageNumber, this.totalRowsPerPage);
      this.invoiceList = result.data.rows;
      this.totalInvoiceCount = result.data.count;
      this.totalPageCount = Math.ceil(this.totalInvoiceCount / this.totalRowsPerPage);
      this.totalPageCountArr = await this.pagination(this.currentPageNumber, this.totalPageCount);
      this.showLoader = false;
    } catch(error) {
      console.log(error)
      this.showLoader = false;
    }
  }

  pagination(c, m) {
    var current = c,
        last = m,
        delta = 2,
        left = current - delta,
        right = current + delta + 1,
        range = [],
        rangeWithDots = [],
        l;

    for (let i = 1; i <= last; i++) {
        if (i == 1 || i == last || i >= left && i < right) {
            range.push(i);
        }
    }

    for (let i of range) {
        if (l) {
            if (i - l === 2) {
                rangeWithDots.push(l + 1);
            } else if (i - l !== 1) {
                rangeWithDots.push('...');
            }
        }
        rangeWithDots.push(i);
        l = i;
    }

    return rangeWithDots;
}
isNumber(number) {
  return isNaN(number);
}
}
